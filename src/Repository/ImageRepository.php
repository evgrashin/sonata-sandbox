<?php declare(strict_types = 1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class ImageRepository extends EntityRepository
{
}