<?php declare(strict_types = 1);

namespace App\Admin;

use App\Entity\BlogPost;
use App\Entity\Category;
use App\Entity\Image;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Sonata\DoctrineORMAdminBundle\Admin\FieldDescription;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
final class BlogPostAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Content', ['class' => 'col-md-9'])
                ->add('title', TextType::class)
                ->add('body', TextareaType::class)
                ->add('image', AdminType::class, [
                    'required' => false,
                    'delete' => false,
                ])
                ->add('image', ModelType::class, [
                    'class' => Image::class,
                    'property' => 'filename',
                    'template' => 'Admin/image_select.html.twig',
                ])
            ->end()
            ->with('Meta data', ['class' => 'col-md-3'])
                ->add('draft', BooleanType::class, [
                    'choices' => [
                        'yes' => true,
                        'no' => false,
                    ],
                ])
                ->add('category', ModelType::class, [
                    'class' => Category::class,
                    'property' => 'name',
                ])
            ->end();

//        $formMapper
//            ->tab('Post')
//                ->with('Content', ['class' => 'col-md-8'])
//                    ->add('title', TextType::class)
//                    ->add('body', TextareaType::class)
//                ->end()
//            ->end()
//            ->tab('Meta data')
//                ->with('Category', ['class' => 'col-md-8'])
//                    ->add('category', ModelType::class, [
//                        'class' => Category::class,
//                        'property' => 'name',
//                    ])
//                ->end()
//            ->end();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('body')
            ->add('draft')
            ->add('category', null, [], EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
            ]);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('category.name')
            ->add('draft')
            ->add('image', null, [
                'template' => 'Admin/image.html.twig',
            ]);
    }

    /**
     * @param mixed $object
     * @return string|null
     */
    public function toString($object)
    {
        return $object instanceof BlogPost
            ? $object->getTitle()
            : 'Blog Post';
    }

    /**
     * @param mixed $page
     */
    public function prePersist($page)
    {
        $this->manageEmbeddedImageAdmins($page);
    }

    /**
     * @param mixed $page
     */
    public function preUpdate($page)
    {
        $this->manageEmbeddedImageAdmins($page);
    }

    /**
     * @param mixed $page
     */
    private function manageEmbeddedImageAdmins($page)
    {
        /** @var FieldDescription $fieldDescription */
        foreach ($this->getFormFieldDescriptions() as $fieldName => $fieldDescription) {
            if ($fieldDescription->getFieldName() === 'image' &&
                ($associationMapping = $fieldDescription->getAssociationMapping()) &&
                $associationMapping['targetEntity'] === 'App\Entity\Image'
            ) {
                $getter = 'get'.$fieldName;
                $setter = 'set'.$fieldName;

                /** @var Image $image */
                $image = $page->$getter();
                if (null !== $image) {
                    if ($image->getFile()) {
                        $image->upload(); // in doc: ->refreshUpdateAt()
                    } elseif (!$image->getFile() && !$image->getFilename()) {
                        $page->$setter(null);
                    }
                }
            }
        }
    }
}