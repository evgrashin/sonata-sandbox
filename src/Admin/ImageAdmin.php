<?php declare(strict_types = 1);

namespace App\Admin;

use App\Entity\Image;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
final class ImageAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Image $image */
        $image = $this->getSubject();

        $fileFieldOptions = ['required' => false];
        if ($image && ($path = $image->getPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$path;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview"/>';
        }

        $formMapper->add('file', FileType::class, $fileFieldOptions);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('image', null, [
                'template' => 'Admin/image.html.twig',
            ]);
    }

    /**
     * @param mixed $image
     */
    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    /**
     * @param mixed $image
     */
    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    /**
     * @param Image $image
     */
    private function manageFileUpload(Image $image)
    {
        $image->upload();
        if ($image->getFile()) {
            $image->refreshUpdated();
        }
    }

    /**
     * @param mixed $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Image
            ? $object->getFilename()
            : 'Blog Post';
    }
}